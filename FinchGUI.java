import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

class FinchGUI {

	public static void main(String[] args) {
	
		JFrame frame = new JFrame("Finch GUI");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(500,350);
		
		JPanel panel = new JPanel(new BorderLayout());
		
		//JLabel label = new JLabel("I am a Label");
		
		JButton move = new JButton("Move");
		JButton exit = new JButton("Exit");
		


		ActionListener al = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		};

		exit.addActionListener(al);


		JTextField myOutput = new JTextField(16);


		myOutput.setText("Hello. Please press Move to move Finch and press Exit to end program.");
		panel.add(move, BorderLayout.CENTER);
		panel.add(exit, BorderLayout.SOUTH);
		panel.add(myOutput, BorderLayout.NORTH);

		
		frame.setContentPane(panel);
		
		frame.setVisible(true);


	}



}
